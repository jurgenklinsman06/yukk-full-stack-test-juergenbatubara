@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">{{ __('Wallet History') }}</div>
                    <H4>Balace: Rp{{ number_format($balance, 2, ',', '.') }}</H4>
                    <div class="card-body">
                        <table class="table table-flush dataTable-table" id="history-table">
                            <div id="routeWallet" data-route="{{ route('wallets') }}">
                            <thead class="thead-light">
                                <tr>
                                    <th class="border-0 rounded-start">ID</th>
                                    <th class="border-0">Transaction ID</th>
                                    <th class="border-0">Transaction Type</th>
                                    <th class="border-0">Amount</th>
                                    <th class="border-0 rounded-end">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Item -->

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
