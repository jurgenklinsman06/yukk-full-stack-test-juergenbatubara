@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ __('Transaction') }}</h4>
                    </div>

                    <div class="card-body">
                        @if (Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        @php
                            $transactionError = $errors->transaction->any();
                            $is_valid = true;

                            if ($transactionError) {
                                $is_valid = false;
                            }
                        @endphp

                        <div class="col-md-7 col-lg-8">
                            <form class="needs-validation" action="{{ route('storeTransaction') }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row g-3">
                                    <div class="col-md-5">
                                        <label for="transactionType" class="form-label">Transaction Type</label>
                                        <select class="form-select" @class(['form-select', 'is-invalid' => !$is_valid]) id="transactionType"
                                            name="transactionType" required>
                                            <option value="">Choose...</option>
                                            <option value="topup">Top Up</option>
                                            <option value="transaction">Transaction</option>
                                        </select>
                                        @if ($transactionError)
                                            <div class="invalid-feedback">
                                                {{ $errors->transaction->first('transactionType') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-12">
                                        <label for="amount" class="form-label">Amount</label>
                                        <input type="number" @class(['form-control', 'is-invalid' => !$is_valid]) id="amount" name="amount" value="{{ old('amount', '') }}>
                                        @if ($transactionError)
                                            <div class="invalid-feedback">
                                                {{ $errors->transaction->first('amount') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-12">
                                        <label for="description" class="form-label">Description</label>
                                        <textarea @class(['form-control', 'is-invalid' => !$is_valid]) id="description" name="description" placeholder="Description" required
                                            rows="3">{{ old('description', '') }}</textarea>
                                        @if ($transactionError)
                                            <div class="invalid-feedback">
                                                {{ $errors->transaction->first('description') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-12">
                                        <label for="file" class="form-label">Transaction file proof</label>
                                        <input @class(['form-control', 'is-invalid' => !$is_valid]) type="file" id="transactionFile"
                                            name="transactionFile">
                                        @if ($transactionError)
                                            <div class="invalid-feedback">
                                                {{ $errors->transaction->first('transactionFile') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <hr class="my-4">
                                <button class="w-100 btn btn-primary btn-lg" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
