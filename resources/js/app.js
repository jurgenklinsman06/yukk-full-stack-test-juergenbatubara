import './bootstrap';
import 'laravel-datatables-vite';

import $ from 'jquery';
window.jQuery = window.$ = $;

import DataTable from 'datatables.net-bs5';
window.DataTable = DataTable;

$(document).ready(function () {
    var route = $("#routeWallet").data('route');
    $('#history-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: route,
        columns: [{
            data: 'id',
            name: 'id'
        },
        {
            data: 'transaction_uuid',
            name: 'transaction_uuid'
        },
        {
            data: 'transaction.direction',
            name: 'transaction.direction'
        },
        {
            data: 'transaction.amount',
            name: 'transaction.amount'
        },
        {
            data: 'transaction.description',
            name: 'transaction.description'
        }
        ]
    });
});