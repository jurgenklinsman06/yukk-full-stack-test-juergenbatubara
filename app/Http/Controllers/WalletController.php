<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\WalletHistoryInterface;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    private $walletHistory;

    public function __construct(WalletHistoryInterface $walletHistory)
    {
        $this->walletHistory = $walletHistory;
    }

    public function index()
    {
        $balance = Auth()->user()->wallet->balance;
        return view('wallet-history', ['balance' => $balance]);
    }

    public function wallets(Request $request)
    {
        if (request()->ajax()) {
            return $this->walletHistory->all();
        }
    }
}
