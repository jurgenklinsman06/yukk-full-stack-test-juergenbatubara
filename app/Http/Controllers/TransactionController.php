<?php

namespace App\Http\Controllers;

use App\Enums\TransactionType;
use App\Repositories\Contracts\TransactionInterface;
use App\Repositories\Contracts\WalletInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    protected $transaction;
    protected $wallet;

    public function __construct(
        TransactionInterface $transaction,
        WalletInterface $wallet
    ) {
        $this->transaction = $transaction;
        $this->wallet = $wallet;
    }

    public function index()
    {
        return view('transaction');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transactionType' => 'required|in:topup,transaction',
            'amount' => 'required|integer|min:1',
            'description' => 'required',
            'transactionFile' => 'required|mimes:pdf,jpg,jpeg,png|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect('/transaction')
                ->withErrors($validator, 'transaction')
                ->withInput();
        }

        $data = [];
        $transactionType = '';
        $userID = Auth()->user()->id;
        if ($request->transactionType == 'topup') {
            $transactionType = TransactionType::TOP_UP->value;
        }

        if ($request->transactionType == 'transaction') {
            $transactionType = TransactionType::TRANSACTION->value;

            $walletUser = $this->wallet->get_user_wallet($userID);

            if (null == $walletUser || $walletUser->balance < $request->amount) {
                return redirect('/transaction')
                    ->with('error', 'Balance Not Enough!!');
            }
        }

        $data['direction'] = $transactionType;
        $data['amount'] = $request->amount;
        $data['description'] = $request->description;

        $file = $request->file('transactionFile');
        $fileName = time() . '_' . $file->getClientOriginalName();
        $filePath = $file->storeAs('transactions', $fileName);
        $data['proof_file'] = $filePath . $fileName;

        $storeTransaction = $this->transaction->store($data);

        if (!$storeTransaction) {
            return redirect('/transaction')
                ->with('error', 'Something Wrong!!');
        }

        return redirect('/transaction')->with('success', 'Transaction Saved!');
    }
}
