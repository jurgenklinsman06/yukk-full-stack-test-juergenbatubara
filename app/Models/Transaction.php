<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'amount',
        'description',
        'proof_file',
        'direction',
        'uuid'
    ];
    public function getDirectionAttribute($value)
    {
        $type = 'Transaction';

        if ($value == 1) {
            $type = "Topup";
        }
        return $type;
    }
}
