<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'wallet_id',
        'transaction_id',
        'transaction_uuid'
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
