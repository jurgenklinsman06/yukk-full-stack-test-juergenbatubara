<?php

namespace App\Enums;

enum TransactionType: int
{
    case TOP_UP = 1;
    case TRANSACTION = -1;
}
