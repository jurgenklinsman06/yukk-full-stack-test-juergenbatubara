<?php

namespace App\Repositories\Contracts;

interface WalletInterface
{
    /**
     * Get's a record by it's ID
     *
     * @param int
     */
    public function get($id);

    public function get_user_wallet(int $userID);
}
