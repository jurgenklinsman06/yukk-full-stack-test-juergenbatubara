<?php

namespace App\Repositories\Contracts;

interface TransactionInterface
{
    /**
     * Get's a record by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all records.
     *
     * @return mixed
     */
    public function all();


    public function store(array $data);
}
