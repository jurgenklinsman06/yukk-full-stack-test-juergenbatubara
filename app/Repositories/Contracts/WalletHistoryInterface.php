<?php

namespace App\Repositories\Contracts;

interface WalletHistoryInterface
{
    /**
     * Get's a record by it's ID
     *
     * @param int
     */
    public function all();
}
