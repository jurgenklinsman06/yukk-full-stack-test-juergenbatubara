<?php

namespace App\Repositories;

use App\Models\WalletHistory;
use Yajra\DataTables\DataTables;
use App\Repositories\Contracts\WalletHistoryInterface;

class WalletHistoryRepository implements WalletHistoryInterface
{
    public function all()
    {
        $walletID = Auth()->user()->wallet->id;
        $histories = WalletHistory::with('transaction')->where('wallet_id', $walletID);
        return DataTables::of($histories)
            ->make();
    }
}
