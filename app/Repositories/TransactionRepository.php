<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\Repositories\Contracts\TransactionInterface;
use Illuminate\Support\Str;

class TransactionRepository implements TransactionInterface
{
    /**
     * Get's a record by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return Transaction::find($id);
    }

    /**
     * Get's all records.
     *
     * @return mixed
     */
    public function all()
    {
    }

    public function store(array $data)
    {
        $data['user_id'] = Auth()->user()->id;
        $data['uuid'] = Str::uuid();
        $transaction = Transaction::create($data);

        $userWallet = Wallet::where('user_id', $data['user_id'])->first();
        if (null == $userWallet) {
            $userWallet = new Wallet();
            $userWallet->user_id = $data['user_id'];
            $userWallet->balance = $data['amount'];
            $userWallet->save();
        } else {
            $userWallet->balance = ($data['amount'] * $data['direction']) + $userWallet->balance;
            $userWallet->save();
        }

        $walletHistory = new WalletHistory();
        $walletHistory->wallet_id = $userWallet->id;
        $walletHistory->transaction_id = $transaction->id;
        $walletHistory->transaction_uuid = $transaction->uuid;
        $walletHistory->save();

        return;
    }
}
