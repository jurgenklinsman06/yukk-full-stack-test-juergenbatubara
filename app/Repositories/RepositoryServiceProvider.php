<?php

namespace App\Repositories;

use App\Repositories\Contracts\TransactionInterface;
use App\Repositories\Contracts\WalletHistoryInterface;
use App\Repositories\Contracts\WalletInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(TransactionInterface::class, TransactionRepository::class);
        $this->app->bind(WalletInterface::class, WalletRepository::class);
        $this->app->bind(WalletHistoryInterface::class, WalletHistoryRepository::class);
    }
}
