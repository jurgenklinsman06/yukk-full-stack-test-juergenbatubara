<?php

namespace App\Repositories;

use App\Models\Wallet;
use App\Repositories\Contracts\WalletInterface;


class WalletRepository implements WalletInterface
{
    /**
     * Get's a record by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return Wallet::find($id);
    }

    public function get_user_wallet(int $userID)
    {
        return Wallet::where('user_id', $userID)->first();
    }
}
